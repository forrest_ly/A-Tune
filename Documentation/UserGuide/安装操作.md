# 安装操作<a name="ZH-CN_TOPIC_0213178454"></a>

安装A-Tune的操作步骤如下：

1.  挂载openEuler的iso文件。

    ```
    # mount openEuler-1.0-aarch64-dvd.iso /mnt
    ```

2.  配置本地yum源。

    ```
    # vim /etc/yum.repos.d/local.repo
    ```

    配置内容如下所示：

    ```
    [local]
    name=local
    baseurl=file:///mnt
    gpgcheck=0
    enabled=1
    ```

3.  安装A-Tune服务端。

    >![](public_sys-resources/icon-note.gif) **说明：**   
    >本步骤会同时安装服务端和客户端软件包，对于单机部署模式，请跳过**步骤4**。  

    ```
    # yum install atune -y
    ```

4.  安装A-Tune客户端。

    ```
    # yum install atune-client -y
    ```

5.  验证是否安装成功。

    ```
    # rpm -qa | grep atune
    atune-client-xxx
    atune-db-xxx
    atune-xxx
    ```

    有如上回显信息表示安装成功。


